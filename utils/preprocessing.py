import itertools
import numpy as np
import time
import sys
from joblib import Parallel, delayed
from scipy.ndimage import convolve1d, rotate, zoom
from scipy.signal.windows import gaussian
from skimage.transform import rescale, resize


def process_nii_obj(
        nii_obj,
        mask_obj,
        patch_size,
        kernel_size,
        n_layers,
        to_interp=True,
        lr_axis=None,
        k=None,
        n_rots=6,
        n_cores=1,
        acq_dim=2,
        train=True,
        noise_level=None,
        window_option=None,
        verbose=True,
        modality="mr",
):
    '''
    Inputs:
        nii_obj : nib.Nifti1Object, object of interest
        nrots : int, one less than number of rotations for training data, default 6 (results in 7 rotations)
        verbose : bool, whether to print state and time calculations
    Outputs:
        imgs_hr_xyz : list of ndarrays, list of rotated hr images
        imgs_blurred_xyz : list of ndarrays, list of rotated
                           images blurred along x-axis
        imgs_aliased_xyz : list of ndarryas, list of rotated
                           images blurred and aliased along x-axis
    '''
    process_st = time.time()
    if verbose:
        print("{} PREPROCESSING IMAGE {}".format("="*10, "="*10))

    par_obj = Parallel(n_jobs=n_cores, prefer="threads")

    ########## 1. Load Image ##########

    img_hr = nii_obj.get_fdata(dtype=np.float32)
    mask_hr = mask_obj.get_fdata(dtype=np.float32)

    acq_resolution = nii_obj.header.get_zooms()
    if k is None:
        k = max(acq_resolution) / min(acq_resolution)

    if verbose:
        print("Calculated resolution ratio: {}".format(k))
    # Round k to hundredths place
    k = round(k, 2)
    if verbose:
        print("Rounded resolution ratio: {}".format(k))

    ########## 1.a. Set coordinate system s.t. z is the LR axis ##########

    # if None, automatically infer
    # otherwise parse chars into ints
    if lr_axis is None:
        lr_axis = np.argmax(acq_resolution)
    elif lr_axis.isalpha():
        axis_dict = {'x': 0, 'y': 1, 'z': 2}
        lr_axis = axis_dict[lr_axis]
    else:
        lr_axis = int(lr_axis)

    # transpose appropriately
    if lr_axis == 0:
        img_hr_xyz = img_hr.transpose(1, 2, 0)
        mask_hr_xyz = mask_hr.transpose(1, 2, 0)
    elif lr_axis == 1:
        img_hr_xyz = img_hr.transpose(2, 0, 1)
        mask_hr_xyz = mask_hr.transpose(2, 0, 1)
    else:
        img_hr_xyz = img_hr
        mask_hr_xyz = mask_hr

    ########## 1.b. Digital upsample ##########

    if to_interp:
        if verbose:
            print("Digitally upsampling...")
        st = time.time()
        if acq_dim == 2:
            img_hr_xyz = spline_interp(img_hr_xyz, k, axis=lr_axis)
            mask_hr_xyz = spline_interp(mask_hr_xyz, k, axis=lr_axis)
        else:
            print("INVALID ACQ_DIM: {}".format(acq_dim))
            sys.exit()
        en = time.time()
        if verbose:
            print(
                "\tElapsed time to digitally upsample: {:.4f}s".format(en-st))

    ########## 1.c. Normalize ##########
    img_hr_xyz, *inv_normalize_params = normalize(img_hr_xyz)
    normalized_mean = img_hr_xyz.mean()

    ########## Concatenate  ##########
    img_hr_xyz = np.stack([img_hr_xyz, mask_hr_xyz], axis=-1)

    ########## 2. Rotate N times along z axis ##########

    if n_rots == 0:
        imgs_hr_xyz = [img_hr_xyz]
    else:
        if verbose:
            print("Rotating in-plane {} times...".format(n_rots))
        st = time.time()
        imgs_hr_xyz = get_n_rots(img_hr_xyz, n_rots=n_rots, n_cores=n_cores)
        en = time.time()
        if verbose:
            print("\tElapsed time to get rotations: {:.4f}s".format(en-st))

    ########## If testing mode, return rotated images ##########
    if not train:
        return imgs_hr_xyz, k, inv_normalize_params

    ########## 3. Degrade to create training pairs ##########

    if verbose:
        print("Applying blur to rotated images...")
    st = time.time()

    blur_k = fwhm_needed(min(acq_resolution), max(acq_resolution))
    imgs_blurred_xyz = par_obj(delayed(blur)(img, blur_k)
                               for img in imgs_hr_xyz)

    en = time.time()
    if verbose:
        print("\tElapsed time to apply blur: {:.4f}s".format(en-st))

    if verbose:
        print("Applying aliasing to blurred images...")
    st = time.time()
    imgs_aliased_xyz = par_obj(delayed(alias)(img, k)
                               for img in imgs_blurred_xyz)
    en = time.time()
    if verbose:
        print("\tElapsed time to apply aliasing: {:.4f}s".format(en-st))

    ########## 4. Optionally Add Noise ##########

    if noise_level is not None and noise_level > 0:
        if verbose:
            print("Adding Poisson and Uniform noise level {} to aliased images...".format(
                noise_level))

        st = time.time()

        # Add to aliased images
        imgs_aliased_xyz = par_obj(delayed(add_noise)(
            img, noise_level) for img in imgs_aliased_xyz)

        en = time.time()
        if verbose:
            print("\tElapsed time to apply noise: {:.4f}s".format(en-st))

    process_en = time.time()
    if verbose:
        print("{} PREPROCESSING FINISHED {}".format("="*10, "="*10))
        print("\tElapsed time to preprocess: {:.4f}s".format(
            process_en-process_st))

    return imgs_hr_xyz, imgs_blurred_xyz, imgs_aliased_xyz


def get_n_rots(img_xyz, n_rots, min_angle=0, max_angle=90, n_cores=1):
    # note: actually `n_rots` + 1 rotations are used
    angles = range(min_angle, max_angle+1, max_angle//n_rots)

    # move z axis to front to iterate over xy slices
    img_zxy = img_xyz.transpose(2, 0, 1, 3)

    # Lambda functions to assist legibility of parallel call
    def slice_rot(sl, angle):
        return rotate(sl, angle, reshape=False, mode='nearest')

    def apply_rot_to_vol(vol, angle):
        # rotate
        rot_vol = np.array(
            [slice_rot(sl, angle) for sl in vol]
        ).transpose(1, 2, 0, 3)
        rot_vol, *_ = normalize(rot_vol)
        return rot_vol

    imgs_xyz = Parallel(n_jobs=n_cores, prefer="threads")(
        delayed(apply_rot_to_vol)(img_zxy, angle) for angle in angles)

    return imgs_xyz


def inv_get_n_rots(imgs_xyz, min_angle=0, max_angle=90, n_cores=1):
    # note: actually `n_rots` + 1 rotations are used
    n_rots = len(imgs_xyz) - 1

    if n_rots == 0:
        # no rotations
        return imgs_xyz
    angles = range(min_angle, max_angle+1, max_angle//n_rots)

    # move z axis to front to iterate over xy slices
    imgs_zxy = [img.transpose(2, 0, 1, 3) for img in imgs_xyz]

    # Lambda functions to assist legibility of parallel call
    def slice_rot(sl, angle): return rotate(
        sl, angle, reshape=False, mode='reflect')

    def apply_rot_to_vol(vol, angle): return np.array(
        [slice_rot(sl, -angle) for sl in vol]
    ).transpose(1, 2, 0, 3)

    imgs_xyz = Parallel(n_jobs=n_cores, prefer="threads")(
        delayed(apply_rot_to_vol)(img_zxy, angle)
        for img_zxy, angle in zip(imgs_zxy, angles))

    return imgs_xyz


def nd_grid(img_size, axis):
    # ---- ndgrid for fft of imgSize ----
    us = np.linspace(-1, 1, img_size[0])
    vs = np.linspace(-1, 1, img_size[1])
    ws = np.linspace(-1, 1, img_size[2])
    u, v, w = np.meshgrid(us, vs, ws)
    u = np.transpose(u, [1, 0, 2])
    v = np.transpose(v, [1, 0, 2])
    w = np.transpose(w, [1, 0, 2])
    axis_dict = {0: u, 1: v, 2: w}
    return axis_dict[axis]


def hamming_window(matrix, scale):
    # ---- Hamming window applied on fft ----
    return 0.54 - 0.46 * np.cos(2 * np.pi * (matrix + 1.0 / scale) * scale / 2.0)


def fermi_window(matrix, scale):
    # ---- Fermi window applied on fft ----
    # Source: Bernstein, Matt A., Sean B. Fain, and Stephen J. Riederer.
    # "Effect of Windowing and Zero-Filled Reconstruction of MRI Data on
    # Spatial Resolution and Acquisition Strategy." Journal of MRI(2001)

    # T is a transition width set to 10 / (N/2) or 0.07813 when N = 256
    T = 10 / (len(matrix) / 2)
    # TODO: normalize `u` s.t. the FWHM occurs at u=1.
    u = np.abs(matrix)  # currently just abs
    return 1.0 / (1.0 + np.exp((u - 1)/T))


def sinc_window(matrix, scale):
    # ---- sinc window applied on fft ----
    return np.sinc(scale * matrix)


def zero_filling_fft_3d(lr_img, scale, axis=2, window_option=None):
    '''
    Modified from Blake's code
    '''
    # --- Simulate MRI zero filling in fft ----

    # 1(1). pad image boundary to increase FOV, and avoid wrap-around
    pads_fov = int(round(scale + 0.49)) - 1
    lr_img_pad = np.pad(lr_img, pads_fov, 'constant', constant_values=0)
    # 1(2). fft
    fft_lr_img = np.fft.fftshift(np.fft.fftn(lr_img_pad))

    # 2. decide pad size for fft
    pads = tuple(itertools.starmap(
        lambda i, dim: (int(round(dim / 2.0 * (scale - 1))),) *
        2 if i == axis else (0, 0),
        enumerate(fft_lr_img.shape),
    ))
    target_size = tuple(itertools.starmap(
        lambda i, dim: int(round(dim * scale)) if i == axis else dim,
        enumerate(lr_img.shape),
    ))

    # 3(1): zero pad fft
    fft_lr_img_pad = np.pad(fft_lr_img, pads, 'constant', constant_values=0)

    # 3(2): if windows need to be applied to avoid ringing
    if window_option is not None:
        grid = nd_grid(fft_lr_img_pad.shape, axis)
        if window_option == 'hamming':
            fft_lr_img_pad = np.multiply(
                fft_lr_img_pad, hamming_window(grid, scale))
        elif window_option == 'sinc':
            fft_lr_img_pad = np.multiply(
                fft_lr_img_pad, sinc_window(grid, scale))
        elif window_option == 'fermi':
            fft_lr_img_pad = np.multiply(
                fft_lr_img_pad, fermi_window(grid, scale))

    # 4(1). ifft to restore image
    lr_img_pad = np.fft.ifftn(np.fft.ifftshift(fft_lr_img_pad))

    # Return to correct energy
    lr_img_pad *= np.prod(lr_img_pad.shape)
    lr_img_pad /= np.prod(lr_img.shape)

    # 4(2). Unpad image boundary
    if lr_img_pad.shape != target_size:
        lr_img_pad = lr_img_pad[
            pads_fov:-pads_fov,
            pads_fov:-pads_fov,
            pads_fov:-pads_fov,
        ]

    # Absolute value
    lr_img_pad = np.abs(lr_img_pad)

    # Clip numerical errors
    #lr_img_pad[np.where(lr_img_pad < 1e-8)] = 0

    # Return to float32
    lr_img_pad = lr_img_pad.astype(np.float32)

    return lr_img_pad


def std_to_fwhm(sigma):
    return 2 * np.sqrt(2 * np.log(2)) * sigma


def fwhm_to_std(gamma):
    return gamma / (2 * np.sqrt(2 * np.log(2)))


def fwhm_needed(fwhm_hr, fwhm_lr):
    return np.sqrt(fwhm_lr**2 - fwhm_hr**2)


def spline_interp(x, k, axis=0):
    down_scale = tuple(
        k if i == axis else 1 for i in range(len(x.shape))
    )
    return zoom(x, down_scale, order=3, mode='nearest', prefilter=True)


def alias(x, k, axis=0):
    epsilon = 1e-8
    
    down_scale = np.array(
        [1/k if i == axis else 1 for i in range(len(x.shape))])
    
    x_ds = rescale(
        image=x, 
        scale=down_scale,
        mode='edge',
        order=1,
        preserve_range=True, 
        anti_aliasing=False,
    )

    x_us = resize(
        image=x_ds,
        output_shape=x.shape,
        mode='edge',
        order=3,
        preserve_range=True, 
        anti_aliasing=False,
    )

    x_us += epsilon
    x_us /= x_us.mean()
    x_us *= x.mean()

    return x_us


def blur(x, k, axis=0):
    epsilon = 1e-8
    # TODO: check if there's gain in this signal
    window = gaussian(int(2*round(k)+1), fwhm_to_std(k))
    blurred = convolve1d(x, window, mode='nearest', axis=axis)
    blurred += epsilon
    blurred /= blurred.mean()
    blurred *= x.mean()
    return blurred


def normalize(x):
    orig_mean = x.mean()
    orig_std = x.std()
    x = (x - orig_mean) / orig_std
    squash_min = x.min()
    squash_max = x.max()
    #x = 2 * (x - squash_min) / (squash_max - squash_min) - 1
    x = (x - squash_min) / (squash_max - squash_min)
    return x, squash_min, squash_max, orig_mean, orig_std


def inv_normalize(x, squash_min, squash_max, orig_mean, orig_std):
    term1 = squash_min
    term2 = (squash_max - squash_min)
    #term3 = (x + 1) / 2
    # return (term1 + (term2 * term3)) * orig_std + orig_mean
    return (term1 + (term2 * x)) * orig_std + orig_mean


def add_noise(x, noise_level):
    '''
    First adds Poisson noise.
    Then adds speckle noise.
    '''

    poisson_noise = np.random.poisson(x)
    uniform_noise = np.random.uniform(low=0.0, high=noise_level, size=x.shape)

    x = x + (poisson_noise * noise_level) + uniform_noise
    x, *_ = normalize(x)

    return x
