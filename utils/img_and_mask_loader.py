from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import nibabel as nib
from tqdm import tqdm

from scipy.ndimage import convolve1d, rotate, zoom
from scipy.signal.windows import gaussian
from skimage.transform import rescale, resize

import torch
import numpy as np
import time
from joblib import Parallel, delayed


def std_to_fwhm(sigma):
    return 2 * np.sqrt(2 * np.log(2)) * sigma


def fwhm_to_std(gamma):
    return gamma / (2 * np.sqrt(2 * np.log(2)))


def fwhm_needed(fwhm_hr, fwhm_lr):
    return np.sqrt(fwhm_lr**2 - fwhm_hr**2)


def target_pad(img, target_dim):

    def get_pads(target_dim, d):
        p = (target_dim - d) // 2
        if p % 2 != 0:
            return (p, p + 1)
        return (p, p)

    pads = tuple(
        get_pads(target_dim, d)
        for d in img.shape
    )

    return np.pad(
        img,
        pads,
        mode='reflect',
    )


def get_n_rots(img_xyz, n_rots, min_angle=0, max_angle=90, n_cores=1):
    # note: actually `n_rots` + 1 rotations are used
    angles = range(min_angle, max_angle+1, max_angle//n_rots)

    # move z axis to front to iterate over xy slices
    img_zxy = img_xyz.transpose(2, 0, 1)

    # Lambda functions to assist legibility of parallel call
    def slice_rot(sl, angle):
        if angle == 0:
            return sl
        elif angle == 90:
            return np.rot90(sl)
        elif angle == 180:
            return np.rot90(sl, k=2)
        elif angle == 270:
            return np.rot90(sl, k=3)
        return rotate(sl, angle, reshape=False, mode='nearest')

    def apply_rot_to_vol(vol, angle):
        # rotate
        rot_vol = np.array(
            [slice_rot(sl, angle) for sl in vol]
        ).transpose(1, 2, 0)
        return rot_vol

    imgs_xyz = Parallel(n_jobs=n_cores, prefer="threads")(
        delayed(apply_rot_to_vol)(img_zxy, angle) for angle in angles)

    return imgs_xyz


class ImageAndMask(Dataset):
    def __init__(
        self,
        img_fpath,
        mask_fpath,
        patch_size,
        n_steps,
        to_interp,
        interp_fpath=None,
        interp_mask_fpath=None,
        n_rots=0,
        kernel_size=3,
        n_cores=1,
        mode='SR',
        train=True
    ):

        par_obj = Parallel(n_jobs=n_cores, prefer="threads")

        self.train = train
        self.n_rots = n_rots
        self.patch_size = patch_size
        self.mode = mode
        self.n_steps = n_steps
        self.return_mask_patch = False
        self.kernel_size = kernel_size

        # Load nifti objs
        nii_obj = nib.load(img_fpath)
        mask_obj = nib.load(mask_fpath)

        # Keep affines
        self.img_affine = nii_obj.affine
        self.mask_affine = mask_obj.affine

        # Keep headers
        self.img_header = nii_obj.header
        self.mask_header = mask_obj.header

        # Read acq dim
        self.acq_res = nii_obj.header.get_zooms()
        self.k = round(max(self.acq_res) / min(self.acq_res), 2)
        self.k = round(self.k, 2)
        self.blur_k = fwhm_needed(min(self.acq_res), max(self.acq_res))

        # Fpaths
        self.img_fpath = img_fpath
        self.mask_fpath = mask_fpath

        # Load img and mask into RAM
        self.img = nii_obj.get_fdata(dtype=np.float32)
        self.mask = mask_obj.get_fdata(dtype=np.float32)

        # Spline interp
        print("Digitally upsampling...")
        st = time.time()
        self.img = self.spline_interp(self.img, order=3) # img is bicubic interp
        self.mask = self.spline_interp(self.mask, order=0) # mask is nn interp
        en = time.time()
        print("\tElapsed time to digitally upsample: {:.4f}s".format(en-st))

        self.orig_shape = self.img.shape

        # Write before normalization
        if interp_fpath is not None and interp_mask_fpath is not None:
            print("Saving interpolated images...")
            self.write_nii(self.img, interp_fpath)
            self.write_nii(self.mask, interp_mask_fpath)

        # Normalize
        print("Normalizing...")
        st = time.time()
        self.img, *self.inv_normalize_params = self.normalize(self.img)
        self.normalized_mean = self.img.mean()
        en = time.time()
        print("\tElapsed time to normalize: {:.4f}s".format(en-st))

        # Rotate
        if self.n_rots == 0:
            self.imgs = [self.img]
            self.masks = [self.mask]
        else:
            print("Rotating in-plane {} times...".format(self.n_rots))
            st = time.time()
            self.imgs = get_n_rots(
                self.img, max_angle=90, n_rots=self.n_rots, n_cores=n_cores)
            self.masks = get_n_rots(
                self.mask, max_angle=90, n_rots=self.n_rots, n_cores=n_cores)
            en = time.time()
            print("\tElapsed time to get rotations: {:.4f}s".format(en-st))

        # Track mask indices
        self.masks_idx = [np.array(np.where(mask != 0)) for mask in self.masks]

        if self.train:
            # Degrade
            print("Degrading...")
            st = time.time()
            self.imgs_blur = par_obj(delayed(self.blur)(img)
                                     for img in self.imgs)
            self.masks_blur = par_obj(delayed(self.blur)(mask)
                                      for mask in self.masks)
            # round the masks back to integer values
            self.masks_blur = [np.around(mask_blur) for mask_blur in self.masks_blur]
            en = time.time()
            print("\tElapsed time to degrade: {:.4f}s".format(en-st))

            # Alias
            print("Aliasing...")
            st = time.time()
            self.imgs_alias = par_obj(delayed(self.alias)(img)
                                      for img in self.imgs_blur)
            self.masks_alias = par_obj(delayed(self.alias)(mask)
                                       for mask in self.masks_blur)
            # round the masks back to integer values
            self.masks_alias = [np.around(mask_alias) for mask_alias in self.masks_alias]
            en = time.time()
            print("\tElapsed time to alias: {:.4f}s".format(en-st))

    def __len__(self):
        return self.n_steps

    def set_mode(self, mode):
        self.mode = mode

    def __getitem__(self, i):
        '''
        To be used with DataLoader during training
        '''
        def random_slice(s, p):
            st = np.random.randint(0, s - p)
            en = st + p
            return slice(st, en)

        def safe_slice(st, en, p, dim):
            if st < 0:
                st += p
                en += p
            elif en >= dim:
                st -= p
                en -= p

            return slice(st, en)

        # Choose a random rotation
        rot_i = np.random.randint(0, len(self.imgs))

        # select patch
        self.return_mask_patch = not self.return_mask_patch
        if self.return_mask_patch:

            # pick a positive mask patch
            mask_idx = self.masks_idx[rot_i]\
                [..., np.random.randint(0, self.masks_idx[rot_i].shape[1])]

            idx = tuple(
                safe_slice(m-(p//2), m+(p//2), p, s)
                if i < len(self.masks[0].shape) - 1 else slice(m, m+1)
                for i, (m, p, s) in enumerate(
                    zip(mask_idx, self.patch_size, self.masks[0].shape)
                )
            )
        else:
            idx = tuple(
                random_slice(s, p)
                for(s, p) in zip(self.imgs[0].shape, self.patch_size)
            )

        
        # Both modes expect alias as input
        img_patch_alias = self.imgs_alias[rot_i][idx].transpose(2, 0, 1)
        mask_patch_alias = self.masks_alias[rot_i][idx].transpose(2, 0, 1)

        if self.mode == 'SR': # SR learns alias -> HR
            img_patch = self.imgs[rot_i][idx].transpose(2, 0, 1)
            mask_patch = self.masks[rot_i][idx].transpose(2, 0, 1)
            return img_patch_alias, mask_patch_alias, img_patch, mask_patch
        elif self.mode == 'AA': # AA learns alias -> blur
            img_patch_blur = self.imgs_blur[rot_i][idx].transpose(2, 0, 1)
            mask_patch_blur = self.masks_blur[rot_i][idx].transpose(2, 0, 1)
            return img_patch_alias, mask_patch_alias, img_patch_blur, mask_patch_blur

    def get_rot_vols(self):
        '''
        During test-time, return the list of rotated images.
        Pad each of them to mitigate edge effects
        '''
        target_dim = int(max(self.orig_shape) + np.floor(self.kernel_size/2))
        # add a little more padding
        buf = 2 * self.kernel_size
        target_dim += buf

        imgs = [target_pad(img, target_dim) for img in self.imgs]
        masks = [target_pad(mask, target_dim) for mask in self.masks]

        return imgs, masks

    def recrop(self, x):
        '''
        Re-crop x based on padding given in `get_rot_vols()`
        '''
        # Re-crop to target shape
        if x.shape != self.orig_shape:
            crop = tuple(
                slice(
                    int(np.abs(a-b) // 2),
                    -int(np.abs(a-b) // 2),
                ) if a != b else
                slice(None, None)
                for a, b in zip(self.orig_shape, x.shape)
            )
            x = x[crop]

        if x.shape != self.orig_shape:
            # In the case of odd-numbered dimension, crop off from the start
            # We can do this due to how we pad during pre-processing
            crop = tuple(
                slice(None, -int(np.abs(a-b))) if a != b else
                slice(None, None)
                for a, b in zip(self.orig_shape, x.shape)
            )
            x = x[crop]
        return x

    def normalize(self, x, a=-1, b=1):
        orig_mean = x.mean()
        orig_std = x.std()
        x = (x - orig_mean) / orig_std
        squash_min = x.min()
        squash_max = x.max()
        x = a + ((x - squash_min)*(b-a)) / (squash_max - squash_min)
        return x, squash_min, squash_max, orig_mean, orig_std, a, b

    def inv_normalize(self, x):
        squash_min, squash_max, orig_mean, orig_std, a, b = self.inv_normalize_params

        tmp = x - a
        tmp *= squash_max - squash_min
        tmp /= b - a
        tmp += squash_min
        tmp *= orig_std
        tmp += orig_mean

        return tmp

    def blur(self, x, axis=0):
        window = gaussian(int(2*round(self.blur_k)+1),
                          fwhm_to_std(self.blur_k))
        window /= window.sum() # remove gain
        blurred = convolve1d(x, window, mode='nearest', axis=axis)
        
        return blurred

    def alias(self, x):
        epsilon = 1e-8

        down_scale = np.array(
            [1/self.k if i == 0 else 1 for i in range(len(x.shape))])

        x_ds = rescale(
            image=x,
            scale=down_scale,
            mode='edge',
            order=1,
            preserve_range=True,
            anti_aliasing=False,
        )

        x_us = resize(
            image=x_ds,
            output_shape=x.shape,
            mode='edge',
            order=3,
            preserve_range=True,
            anti_aliasing=False,
        )

        return x_us

    def spline_interp(self, x, order=3):
        down_scale = tuple(
            self.k if i == 2 else 1 for i in range(len(x.shape))
        )
        return zoom(x, down_scale, order=order)

    def write_nii(self, x, out_fpath):
        '''
        Write x as a nifti object.
        Uses the image's affine and header. This should be the same
        as the mask's; if not, there's an issue in the mask creation (outside of
        this program).
        '''
        # Update affine for super-resolved version
        new_scales = (1, 1, 1/self.k)
        new_affine = np.matmul(self.img_affine, np.diag(new_scales + (1,)))
        out_obj = nib.Nifti1Image(
            x,
            affine=new_affine,
            header=self.img_header,
        )
        nib.save(out_obj, out_fpath)
