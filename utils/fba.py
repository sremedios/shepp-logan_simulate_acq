import numpy as np
from joblib import Parallel, delayed

def fba(imgs, p='infinity', n_cores=6):
    par_obj = Parallel(n_jobs=n_cores, prefer="threads")
    
    # put images in Fourier domain
    vs_hat = par_obj(delayed(np.fft.fftn)(img) for img in imgs)
    
    if p == "infinity" or p == 'inf':
        out_img = np.max(vs_hat, axis=0)
    else:
        # calculate fourier magnitude weights
        denominator = np.sum([np.abs(v_hat) ** p for v_hat in vs_hat], axis=0)
        ws = [(np.abs(v_hat) ** p) / denominator for v_hat in vs_hat]
        
        out_img = np.sum([w * v_hat for w, v_hat in zip(ws, vs_hat)], axis=0)
        
    # return to image domain
    out_img = np.fft.ifftn(out_img)
    
    # extract real component
    out_img = out_img.real.astype(np.float32)
    
    return out_img