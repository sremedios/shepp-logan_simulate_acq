from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from PIL import Image
from tqdm import tqdm

from .preprocessing import *

import torch
import numpy as np
import pickle


class LSFMSlice(Dataset):
    def __init__(
        self, 
        img_dir, 
        stat_dict, 
        patch_size, 
        acq_res,
        noise_level=None, 
        mode='SR',
    ):
        self.img_dir = img_dir
        self.stat_dict = stat_dict
        self.fpaths = sorted(img_dir.iterdir())
        self.patch_size = patch_size
        self.k = round(max(acq_res) / min(acq_res), 2)
        self.blur_k = fwhm_needed(min(acq_res), max(acq_res))
        self.noise_level = noise_level
        self.mode = mode

    def __len__(self):
        return len(self.fpaths)

    def __getitem__(self, i):
        # load slice into RAM
        img = Image.open(self.fpaths[i])
        img = np.array(img, dtype=np.float32)

        # pull 1 random patch from the image
        x_st = np.random.randint(0, img.shape[0]-self.patch_size[0])
        x_en = x_st + self.patch_size[0]

        y_st = np.random.randint(0, img.shape[1]-self.patch_size[1])
        y_en = y_st + self.patch_size[1]

        img = img[x_st:x_en, y_st:y_en]

#         # center mean
#         img /= img.mean()
#         img *= self.stat_dict['mean']
        
        # Normalize
        img = (img - self.stat_dict['mean']) / self.stat_dict['std']
        numerator = img - self.stat_dict['min']
        denominator = self.stat_dict['max'] - self.stat_dict['min']
        img = numerator / denominator
        
        # Degrade 
        img_blur = blur(img, self.blur_k)
        
        # Alias
        img_alias = alias(img_blur, self.k)
        
        # Noise
        if self.noise_level is not None and self.noise_level > 0:
            img_alias = add_noise(img_alias, self.noise_level)
            
        if self.mode == 'SR':
            return img_alias, img

        return img_blur, img
    
    def inv_normalize(self, img):
        a = self.stat_dict['min']
        b = self.stat_dict['max'] - self.stat_dict['min']
        c = self.stat_dict['std']
        d = self.stat_dict['mean']
        
        return (a + (b * img)) * c + d
    
def get_mean_std(fpath):
    img = np.array(Image.open(fpath), dtype=np.float32)
    return img.mean(), img.std()

def get_max_min(fpath, mean):
    img = np.array(Image.open(fpath), dtype=np.float32)
    # Center mean to calculate robust max and min
    img /= img.mean()
    img *= mean
    return img.max(), img.min()
    
def write_region_stats(in_dir, out_fpath, n_cores=1):
    '''
    Calculates region statistics for homogeneous normalization.
    Since all images corresponding to a volume are in the same dir
    we calculate by skimming through all images in that dir and write to a file.
    
    Pickled as a dictionary.
    '''
    par_obj = Parallel(n_jobs=n_cores, prefer="threads")
    
    fpaths = sorted(in_dir.iterdir())
 
    means_stds = par_obj(delayed(get_mean_std)(fpath) for fpath in fpaths)
    region_mean, region_std = np.mean(means_stds, axis=0)
    
    maxs_mins = par_obj(delayed(get_max_min)(fpath, region_mean) for fpath in fpaths)
    region_max = np.max(maxs_mins)
    region_min = np.min(maxs_mins)
            
    stat_dict = {
        'mean': region_mean,
        'std': region_std,
        'max': region_max,
        'min': region_min,
    }
    with open(out_fpath, 'wb') as f:
        pickle.dump(stat_dict, f)
        
def read_region_stats(fpath):
    with open(fpath, 'rb') as f:
        stat_dict = pickle.load(f)
    return stat_dict
    