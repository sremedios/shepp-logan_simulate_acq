# shepp-logan_simulate_acq

Simulate different 2D acquisition thicknesses for the Shepp-Logan phantom.

Install the conda environment with
`conda env create -f environment.yml`

The Jupyter Notebook creates Shepp-Logan phantoms and masks at various resolutions, simulating acquisition along axial, sagittal, and coronal planes.
